/*
  This file is part of the MKR NB library.
  Copyright (c) 2018 Arduino SA. All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "Modem.h"

#include "NBUdp.h"

NBUDP::NBUDP() :
  _socket(-1),
  _packetReceived(false),
  _txIp((uint32_t)0),
  _txHost(NULL),
  _txPort(0),
  _txSize(0),
  _rxIp((uint32_t)0),
  _rxPort(0),
  _rxSize(0),
  _rxIndex(0)
{
  MODEM.addUrcHandler(this);
}

NBUDP::~NBUDP()
{
  MODEM.removeUrcHandler(this);
}

uint8_t NBUDP::begin(uint16_t port)
{
  String response;

  
  MODEM.send("AT+QIOPEN=1,2,\"UDP SERVICE\",\"127.0.0.1\",0,3030,0");
  MODEM.waitForResponse(2000,&response);
  Serial.println(response);
  Serial.println("---------------------------------------------");
  _socket=2;
  return 1;
/*
  MODEM.send("AT+USOCR=17");

  if (MODEM.waitForResponse(2000, &response) != 1) {
    return 0;
  }

  _socket = response.charAt(response.length() - 1) - '0';

  MODEM.sendf("AT+USOLI=%d,%d", _socket, port);
  if (MODEM.waitForResponse(10000) != 1) {
    stop();
    return 0;
  }
  */

  return 1;
}

void NBUDP::stop()
{
  if (_socket < 0) {
    return;
  }

  MODEM.sendf("AT+USOCL=%d", _socket);
  MODEM.waitForResponse(10000);

  _socket = -1;
}

int NBUDP::beginPacket(IPAddress ip, uint16_t port)
{
  if (_socket < 0) {
    return 0;
  }

  _txIp = ip;
  _txHost = NULL;
  _txPort = port;
  _txSize = 0;

  return 1;
}

int NBUDP::beginPacket(const char *host, uint16_t port)
{
  if (_socket < 0) {
    return 0;
  }

  _txIp = (uint32_t)0;
  _txHost = host;
  _txPort = port;
  _txSize = 0;

  return 1;
}

int NBUDP::endPacket()
{
  String command;

  if (_txHost != NULL) {
    command.reserve(26 + strlen(_txHost) + _txSize * 2);
  } else {
    command.reserve(41 + _txSize * 2);
  }

  //AT+QISEND=2,10,“10.7.89.10”,6969

  //command += "AT+USOST=";
  command += "AT+QISEND=";
  command += _socket;
  command += ",",
  command += _txSize;
  command += ",\"";

  if (_txHost != NULL) {
    command += _txHost;
  } else {
    command += _txIp[0];
    command += '.';
    command += _txIp[1];
    command += '.';
    command += _txIp[2];
    command += '.';
    command += _txIp[3];
  }

  command += "\",";
  command += _txPort;
  //command += ",",
 // command += _txSize;
  //command += ",\"";
  MODEM.send(command);
  command="";
  MODEM.waitForResponse();

  for (size_t i = 0; i < _txSize; i++) {
    byte b = _txBuffer[i];

    byte n1 = (b >> 4) & 0x0f;
    byte n2 = (b & 0x0f);

    command += (char)(n1 > 9 ? 'A' + n1 - 10 : '0' + n1);
    command += (char)(n2 > 9 ? 'A' + n2 - 10 : '0' + n2);
  }

  command += "\"";

  MODEM.send(command);

  if (MODEM.waitForResponse() == 1) {
    return 1;
  } else {
    return 0;
  }
}

size_t NBUDP::write(uint8_t b)
{
  return write(&b, sizeof(b)); //hallo
}

size_t NBUDP::write(const uint8_t *buffer, size_t size)
{
  if (_socket < 0) {
    return 0;
  }

  size_t spaceAvailable = sizeof(_txBuffer) - _txSize;

  if (size > spaceAvailable) {
    size = spaceAvailable;
  }

  memcpy(&_txBuffer[_txSize], buffer, size);
  _txSize += size;

  return size;
}

int NBUDP::parsePacket()
{
  MODEM.poll();

  if (_socket < 0) {
    return 0;
  }

  if (!_packetReceived) {
    return 0;
  }
  _packetReceived = false;

  String response;

  //MODEM.sendf("AT+USORF=%d,%d", _socket, sizeof(_rxBuffer));
  MODEM.sendf("AT+QIRD=%d", _socket);
  if (MODEM.waitForResponse(10000, &response) != 1) {
    return 0;
  }

  if (!response.startsWith("+QIRD: ")) {   //+QIRD: 4,“10.7.76.34”,7687 <cr><lr> data |  +USORF: 3,"151.9.34.66",2222,len,"16 bytes of data" OK
    return 0;
  }

  int ipStartIndex=response.indexOf('"');
  if (ipStartIndex == -1) {
    return 0;
  }
  //Serial.print("ipStartIndex: "); Serial.println(ipStartIndex);

  response.remove(0, ipStartIndex+1);

  int firstQuoteIndex = response.indexOf('"');
  if (firstQuoteIndex == -1) {
    return 0;
  }

  String ip = response.substring(0, firstQuoteIndex);
  Serial.print("IP: "); Serial.println(ip);
  _rxIp.fromString(ip);

  response.remove(0, firstQuoteIndex + 2);

  int firstCommaIndex = response.indexOf('\r');
  if (firstCommaIndex == -1) {
    return 0;
  }

  String port = response.substring(0, firstCommaIndex);
  Serial.print("Port: "); Serial.println(port);
  _rxPort = port.toInt();
  firstQuoteIndex = response.indexOf("\n");

  //Serial.print("Response: |"); Serial.print(response); Serial.println("|");

  response.remove(0, firstQuoteIndex + 1);
  //response.remove(response.length() - 1);

  Serial.print("Response: |"); Serial.print(response); Serial.println("|");

  _rxIndex = 0;
  _rxSize = response.length() / 2;

  for (size_t i = 0; i < _rxSize; i++) {
    byte n1 = response[i * 2];
    byte n2 = response[i * 2 + 1];

    if (n1 > '9') {
      n1 = (n1 - 'A') + 10;
    } else {
      n1 = (n1 - '0');
    }

    if (n2 > '9') {
      n2 = (n2 - 'A') + 10;
    } else {
      n2 = (n2 - '0');
    }

    _rxBuffer[i] = (n1 << 4) | n2;
  }

  MODEM.poll();

  return _rxSize;
}

int NBUDP::available()
{
  if (_socket < 0) {
    return 0;
  }

  return (_rxSize - _rxIndex);
}

int NBUDP::read()
{
  byte b;

  if (read(&b, sizeof(b)) == 1) {
    return b;
  }

  return -1;
}

int NBUDP::read(unsigned char* buffer, size_t len)
{
  size_t readMax = available();

  if (len > readMax) {
    len = readMax;
  }

  memcpy(buffer, &_rxBuffer[_rxIndex], len);

  _rxIndex += len;

  return len;
}

int NBUDP::peek()
{
  if (available() > 1) {
    return _rxBuffer[_rxIndex];
  }

  return -1;
}

void NBUDP::flush()
{
}

IPAddress NBUDP::remoteIP()
{
  return _rxIp;
}

uint16_t NBUDP::remotePort()
{
  return _rxPort;
}

void NBUDP::handleUrc(const String& urc)
{
  if (urc.startsWith("+QIURC: \"recv\",")) {   //UUSORF   //+UUSORF: <socket>,<length> +UUSORF: 3,16
  //+QIURC: “recv”,<connectID>
    int socket = urc.charAt(urc.length() - 1) - '0';
    Serial.print("Received Socket: "); Serial.println(socket);

    if (socket == _socket) {
      _packetReceived = true;
    }
  } else if (urc.startsWith("+QIURC: \"closed\",")) {     //Close Socket +UUSOCL: <socket> +UUSOCL: 2
  //+QIURC: “closed”,<connectID>
    int socket = urc.charAt(urc.length() - 1) - '0';

    if (socket == _socket) {
      // this socket closed
      _socket = -1;
      _rxIndex = 0;
      _rxSize = 0;
    }
  }
}
